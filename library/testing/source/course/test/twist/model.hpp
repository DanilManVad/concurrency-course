#pragma once

#include "body.hpp"

#include <twist/build.hpp>

#include <chrono>

namespace course::twist {

namespace model {

constexpr bool Supported() {
  return ::twist::build::IsolatedSim();
}

constexpr std::chrono::seconds TimeBudget() {
  return std::chrono::minutes(3);
}

struct Params {
  std::optional<size_t> max_preemptions;
  std::optional<size_t> max_steps;
  bool spurious_wakeups = false;
  bool spurious_failures = false;
};

void Check(TestBody body, Params params);

}  // namespace model

}  // namespace course::twist
