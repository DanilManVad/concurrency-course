#pragma once

#include <functional>

namespace course::twist {

using TestBody = std::function<void()>;

}  // namespace course::twist
